#this test for Rent it and Build it 
# for running test 
npm test 

All test related files in Acceptance test folder


for testing hosts :
https://cancer-alleviator.herokuapp.com/
reference for source code: https://github.com/rquellh/testcafe-cucumber

For a testing we are used:
Cucumber js -  for Gherkin scenarios
TestCafe js -   for simulation browser
Node js - for writing scenarios

For run test first install npm packet 
# npm install
Then you can run test with 
# npm test 
 If you want to run special feature test 
For example 
npm test .\features\cancer_testing_page.feature