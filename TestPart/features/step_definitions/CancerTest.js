

const {Given, When, Then} = require('cucumber');
const Selector = require('testcafe').Selector;
const loginpage = require('../support/pages/cancer_page');


Given(/^I open the login page$/, async function() {
    await testController.navigateTo(loginpage.login.url());
});
When('I am typing username request {string}', async function(text) {
    await testController.typeText(loginpage.login.login_text(), text);
});
Then('I am typing password request {string}', async function(text) {
    await testController.typeText(loginpage.login.login_pass(), text);
});
Then('clicking login button', async function () {
    await testController.click(loginpage.login.loginButton() );

  });
  Then('clicking patient lists first element', async function () {
    await testController.click(loginpage.login.list_element() );

  });
  Then('close patient info', async function () {
    await testController.click(loginpage.login.close() );

  });



  Then('clicking add isikood button', async function () {
    await testController.click(loginpage.login.addIsukood() );

  });





  Then('adding cancer type',async function () {
    await testController.click(loginpage.login.selectCancer() );
  });



  Then('clicking add cancer button',async function () {
    await testController.click(loginpage.login.addCancerType() );
  });
  Then('family doctor should start testing',async function () {
    await testController.click(loginpage.login.startTest());
  })


  Then('adding isikood {string}', async function(text) {
    await testController.typeText(loginpage.login.isukood(), text);
});



  Given(/^I open the Register page$/, async function() {
    await testController.navigateTo(loginpage.login.url_register());
});
When('I am typing Firstname {string}', async function(text) {
    await testController.typeText(loginpage.login.FirstName(), text);
});
Then('I am typing Lastname {string}', async function(text) {
    await testController.typeText(loginpage.login.LastName(), text);
});
Then('I am typing email {string}', async function(text) {
  await testController.typeText(loginpage.login.email(), text);
});
Then('I am typing username {string}', async function(text) {
  await testController.typeText(loginpage.login.RUsername(), text);
});
Then('I am typing password {string}', async function(text) {
  await testController.typeText(loginpage.login.Rpass(), text);
});
Then('I am typing repeat password {string}', async function(text) {
  await testController.typeText(loginpage.login.Reppass(), text);
});
Then('clicking register button', async function () {
  await testController.click(loginpage.login.Rgbutton() );

});
Then('clicking Testing link', async function () {
  await testController.click(loginpage.login.Testlink() );

});
Then('adding symptoms', async function () {
  await testController.click(loginpage.login.symptoms() );
  


});
Then('clicking add symptoms button', async function () {
  await testController.click(loginpage.login.addSym() );

});
Then('adding testname', async function () {
  await testController.click(loginpage.login.testName() );
  


});
Then('adding testresult', async function () {
  await testController.click(loginpage.login.testResult() );

});
Then('clicking add test button', async function () {
  await testController.click(loginpage.login.addTest() );

});
Then('clicking logout button', async function () {
  await testController.click(loginpage.login.logout() );

});

/////////oncologist view 

Then('go oncologist view', async function () {
  await testController.click(loginpage.login.ONC_view() );
});
Then('go Mypatients view', async function () {
  await testController.click(loginpage.login.Mypatients() );
});