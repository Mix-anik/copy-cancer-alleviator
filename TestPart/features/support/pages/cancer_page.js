const {Selector} = require('testcafe');

// Selectors

function select(selector) {
    return Selector(selector).with({boundTestRun: testController});
}

exports.login= {
    url: function() {
        return 'https://cancer-alleviator.herokuapp.com/';
    },
    url_register: function() {
        return 'https://cancer-alleviator.herokuapp.com/register/';
    },
   
    login_text: function() {
        return select('input#uname');
    },

    isukood: function() {
        return select('input#ID-input');
    },

    ONC_view: function() {
        return select('html > body > nav > a:nth-of-type(3)');
    },
    Mypatients: function() {
        return select('html > body > nav > a:nth-of-type(2)');
    },
    login_pass: function() {
        return select("input#pswd");
    },
    loginButton: function () {

        return select('button#btn-login');
    },
    list_element: function () {

        return select('table#patients-table > tbody > tr > td:nth-of-type(3)');
    },
    addIsukood: function () {

        return select('button#Add-ID');
    },
    selectCancer: function () {

        return select('select#cancer-type-input');
    },
    addCancerType: function () {

        return select('button#Add-cancer-type');
    },
    startTest: function () {

        return select('button#testing-test-button');
    },
   close:function(){
       return select('div#modal > div > button');
   },
   FirstName:function(){
    return select('input#fname');
},
LastName:function(){
    return select('input#lname');
},
email:function(){
    return select('input#email');
},
RUsername:function(){
    return select('input#uname');
},
Rpass:function(){
    return select('input#pswd');
},
Reppass:function(){
    return select('input#pswd_repeat');
},


Rgbutton:function(){
    return select('button#btn-registration');
},
Testlink:function(){
    return select('html > body > nav > a:nth-of-type(2)');
},
symptoms:function(){
    return select('input#Symptom-input');
},
addSym:function(){
    return select('button#Add-Symptom')
},
testName:function(){
    return select('input#Test-name-input');
},
testResult:function(){
    return select('input#Test-result-input');
},
addTest:function(){
    return select('button#Add-Test');
},
logout:function(){
    return select('html > body > nav > a:nth-of-type(4)');
},
};
