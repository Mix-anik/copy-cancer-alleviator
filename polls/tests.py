from django.test import TestCase


class SimpleTest(TestCase):
    def test_index(self):
        resp = self.client.get('/register/')
        self.assertEqual(resp.status_code, 200)

    def test_index(self):
        resp = self.client.get('/polls/')
        self.assertEqual(resp.status_code, 404)

    def test_index(self):
        resp = self.client.get('/patients/')
        self.assertEqual(resp.status_code, 302)


    def test_index(self):
        resp = self.client.get('/logout/')
        self.assertEqual(resp.status_code, 302)

    def test_index(self):
        resp = self.client.get('/testing/')
        self.assertEqual(resp.status_code, 302)


from .models import Patient,Pathway

class ModelTest(TestCase):

	def create_patient(self, nid="only", name="ismayil",status="okey",age="21",phone="1234556",email="tax@mail.ru"):
		return Patient.objects.create(nid=nid, name=name,status=status,age=age,phone=phone,email=email)

	def test_patient(self):
		w = self.create_patient()
		self.assertTrue(isinstance(w, Patient))




	

# model mommy

from model_mommy import mommy

class PatientTestMommy(TestCase):

	def test_patient_creation_mommy(self):
		what = mommy.make(Patient)
		self.assertTrue(isinstance(what, Patient))
class PathwayTestMommy(TestCase):

	def test_patient_creation_mommy(self):
		what = mommy.make(Pathway)
		self.assertTrue(isinstance(what, Pathway))
