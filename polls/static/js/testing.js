$(function(){
	$("#Add-ID").click(function(){
		if($("#ID-input").val() != "" && $("#cancer-type-input").val() != "") $("#testing-test-button").prop('disabled', false);

		if($("#ID-input").val()!=""){
			$("#patient-chosen").text($("#ID-input").val());
			$("#patient-chosen").addClass("show");
			$("#Add-ID").addClass("tab");
			$("#ID-input").addClass("tab");
			$("#ID-question").addClass("tab");
		};
	});

	$("#Add-cancer-type").click(function(){
		if($("#ID-input").val() != "" && $("#cancer-type-input").val() != "") $("#testing-test-button").prop('disabled', false);

		let cancer_type = $("#cancer-type-input").val();
		$("#cancer-type-chosen").text($("#cancer-type-input").val());
		$("#cancer-type-chosen").addClass("show");
		$("#Add-cancer-type").addClass("tab");
		$("#cancer-type-input").addClass("tab");
		$("#cancer-type-question").addClass("tab");

		$.ajax({
			url: '/testing/',
			dataType: 'text',
			type: 'POST',
			data: cancer_type,
			success: function(response) {
				var data = JSON.parse(response);

				console.log(data);
				var symptoms = data['symptoms']
				var tests = data['tests']


				for(i = 0; i < symptoms.length; i++)
				{
					var symptom = symptoms[i]
					$("#"+cancer_type+" .column").append("<div class='test-symptom'><label for='"+symptom+"'>"+symptom+"</label><input id='"+symptom+"' name='checkbox-form' type='checkbox' value='"+symptom+"'></div>")
				}

				for(i = 0; i < tests.length; i++)
				{
					var test = tests[i]
					$("#"+cancer_type+" .mandatory-fields").append("<div class='test-test'><label for='"+test+"'>DRE</label><input id='"+test+"' name='checkbox-form' type='number' step='0.1'></div>")
				}
			},
			error: function(error) {
				console.log(error)
			}
		});

		if(cancer_type=="Prostate-cancer"){
			$("#Prostate-cancer").addClass("active");
		}else if(cancer_type=="Colorectal-cancer"){
			$("#Colorectal-cancer").addClass("active");
		}else if(cancer_type=="Breast-cancer"){
			$("#Breast-cancer").addClass("active");
		}

	});

	$(".modal-exit").click(function(){
	  $(".modal-info").removeClass("active");
    });

	 $("#ID-question").click(function(){
		$(".modal-info p").text("Insert the national ID of the patient tested and click the add national ID button");
		$(".modal-info").addClass("active");
		$(".modal-info").position({of : this});

	});
	$("#cancer-type-question").click(function(){
		$(".modal-info p").text("Select a cancer type you wish to test for and click Select cancer type button");
		$(".modal-info").addClass("active");
		$(".modal-info").position({of : this});
	});

});

$(function(){
	$("#testing-test-button").click(function(){

		var symptoms = []
		var tests = []

		$(".active input[type='checkbox']").each(function () {
			var symptom = {
				name: $(this).val(),
				value: this.checked
			}

    	symptoms.push(symptom)
		});

		$(".active input[type='number']").each(function () {
			var test = {
				name: $(this).attr('id'),
				value: $(this).val()
			}

    	tests.push(test)
		});

		//console.log(symptoms);
		//console.log(tests);

		var data = {
			nid : $("#ID-input").val(),
			type : $("#cancer-type-input").val(),
			symptoms : symptoms,
			tests : tests
		};

		console.log(data);

			//$("div#testing-form-insert").css("display", "none");
			$("div.testing-results").css("display", "block");

			var dataJSON = JSON.stringify(data);

			$("input#test-content").attr("value", dataJSON);
			$("input#test-content").val(dataJSON);

			$.ajax({
				url: '/testing/',
				dataType: 'text',
				type: 'POST',
				data: dataJSON,
				success: function(response) {
					var result = JSON.parse(response);
					console.log(result);

					$("#resulttable").append("<tr><th id='type'>"+ result.type +"</th><th id='value'>"+ result.value +"</th></tr>")
				},
				error: function(error) {
					console.log(error)
				}
			});

	});
});

$(function(){
	$("button#cancel").click(function(){
		$(location).attr('href', '/testing');
	});
});

$(function(){
	$("button#save").click(function(){
		//console.log($("input#test-content").val());
		nid = $("#ID-input").val();
		cType = $("#resulttable #type").text();
		cValue = $("#resulttable #value").text();

		data = {
			'nid': nid,
			'type': cType,
			'value': cValue
		}

		var dataJSON = JSON.stringify(data);

		$.ajax({
			url: '/patients/',
			dataType: 'text',
			type: 'POST',
			data: dataJSON,
			success: function(response) {
					if (response == "Success")
					{
						console.log("Patient info updated!")
					}
					else if (response == "Error")
					{
						console.log("An Error has occur!");
					}
				},
			error: function(error) {
				console.log(error)
			}
		});
	});
});


$(function () {
    $.ajaxSetup({
        headers: { "X-CSRFToken": $.cookie('csrftoken') }
    });
});
