$(document).ready(function() {
    $('#registration-form').validate({

        //Form field's rules
        rules: {
            fname: {
                required: true
            },
            lname: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            uname: {
                required: true
            },
            pswd: {
                required: true
            },
            pswd_repeat: {
                required: true
            }
        },
        // Messages to be present if rules were not fulfilled
        messages: {
            fname: {
                required: "Please enter your first name."
            },
            lname: {
                required: "Please enter your last name."
            },
            email: {
                required: "Please enter your email.",
                email: "Email address is not valid."
            },
            uname: {
                required: "Please ente user name."
            },
            pswd: {
                required: "Please enter password."
            },
            pswd_repeat: {
                required: "Please repeat password."
            }
        },

        submitHandler: function (form) {
            $.ajax({
                url: "../register/",
                type: "POST",
                data: $("#"+form.id).serialize(),

                success: function (data) {
                    // Checks if data is not empty
                    if(data != null) {
                        // Clears all fields after successful message sending
                        $("#"+form.id)[0].reset();
                        //console.log(data);
                        document.getElementById("registration-inform").innerText = "Created new account!";
                    }
                    else {
                        document.getElementById("registration-inform").innerText = "Data is empty";
                    }
                },

                error: function () {
                    document.getElementById("registration-inform").innerText = "Error";
                },
            });
            return false;
        }
    });
});
