$(function(){
  $("td").each(function(indeks,element){ //coloring status elements
	  if($(element).text()=="Enrolled"){
		$(element).css("color","green");
	  }
	  else if($(element).text()=="Cancelled"){
		$(element).css("color","red");
	  }
	  else if($(element).text()=="Waiting"){
		$(element).css("color","yellow");
	  }
	  else if($(element).text()=="Closed"){
		$(element).css("color","black");
	  }
  });

  $("tr").click(function(){
	  let name = $(this).children(1).html();
	  let ID = 39809109356;
	  let age = 36;
	  let phone_nr = "37255667788";
	  let Email= "kadri.miller@gmail.com";

	  $("#modal-name").text(name);
	  $("#modal-ID").text(ID);
	  $("#modal-Age").text(age);
	  $("#modal-Phone").text("+"+phone_nr);
	  $("#modal-Email").text(Email);
	  $("#modal").addClass("active");
  });

  $(".modal-exit").click(function(){
	  $("#modal").removeClass("active");
  });
});

// Function, which performs filtering on patients by their attributes
function multiFilter() {
	$(document).ready(function () {
		var filterName = $("#searchName").val();
		var filterId = $("#searchId").val();
		var filterStatus = $("#searchStatus").val();

		/*
		console.log("Filter Values:");
		console.log(filterName);
		console.log(filterId);
		console.log(filterStatus);
		console.log("=========================");
		*/
		var tr = $("#patients-table tr");

		tr.each(function(index, tr_elem) {
			if(index != 0) {
				var tds = $(tr_elem).find("td");

				var name = tds[0].innerText.toUpperCase();
				var nid = tds[1].textContent.toUpperCase();
				var status = tds[2].textContent.toUpperCase();

				/*
				console.log("Patient info:");
				console.log(name);
				console.log(nid);
				console.log(status);
				console.log("=========================");
				*/
				var filterNameRes = filterName == "" ? true : name.indexOf(filterName.toUpperCase()) > -1;
				var filterIdRes = filterId == "" ? true : nid.indexOf(filterId) > -1;
				var filterStatusRes = filterStatus == "ALL" ? true : status.indexOf(filterStatus.toUpperCase()) > -1;
				/*
				console.log("Filter Results:");
				console.log(filterNameRes);
				console.log(filterIdRes);
				console.log(filterStatusRes);
				console.log("=========================");
				*/
				tds.each(function(index, td_elem) {
					if (filterNameRes && filterIdRes && filterStatusRes) {
						$(tr_elem).css('display', "");
					}
					else {
						$(tr_elem).css('display', "none");
					}
				})
			}
		})
	});
}
