$(function(){

	//create parameters for search

	$(".Oncology-patients-table th").each(function( index ) { //create parameters for search
	$("#search-type").append("<option value=\""+index+"\">"+$( this ).text()+"</option>");
	});

	//create parameters for search end

	//modal
	$(".Oncology-patient-modal .exit-button").click(function(){ //exit button
		$(".Oncology-patient-modal").removeClass("active");
	});
	$(".Oncology-patients-table tbody tr").click(function(){ //patient chosen
		let name=$(this).children(":nth-child(1)").text();
		let vahituup=$(this).children(":nth-child(6)").text();
		let vanus=$(this).children(":nth-child(3)").text();
		$(".Oncology-patient-modal #name").text(name);
		$(".Oncology-patient-modal #vahituup").text(vahituup);
		$(".Oncology-patient-modal #vanus").text(vanus);
		$(".Oncology-patient-modal").addClass("active");
	});
	//modal end
});

// Assigning patient to doctor
$("#take_patient").click(function(){
	var nid = $(".Oncology-patients-table tbody tr").children(":nth-child(2)").text();

	$.ajax({
		url: '/oncologist_patients/',
		dataType: 'text',
		type: 'POST',
		data: nid,
		success: function(response) {
			//TODO: Update table "oncologist patient"
			console.log(response);
		},
		error: function(error) {
			console.log(error)
		}
	});
});

// Rejecting patient
$("#take_patient").click(function(){

});

$(document).ready(function () {
	$("#dateFrom").datepicker({
		dateFormat: 'dd/mm/yy',
		changeMonth: true,
    changeYear: true,
	});

	$("#dateTo").datepicker({
		 dateFormat: 'dd/mm/yy',
		 changeMonth: true,
     changeYear: true,
	 });
});


// Function, which performs filtering on patients by their attributes
function multiFilter() {
	$(document).ready(function () {
		var filterName = $("#name_filter").val();
		var filterId = $("#nid_filter").val();
		var filterAgeFrom = $("#age_from_filter").val();
		var filterAgeTo = $("#age_to_filter").val();
		var filterStatus = [];
		var filterType = $("#cancer_type_filter").val();
		var filterMy = $("#my_filter").prop('checked');
		var filterDateFrom = $("#dateFrom").datepicker('getDate');
		var filterDateTo = $("#dateTo").datepicker('getDate');

		if($("#accepted").prop('checked')) filterStatus.push("ACCEPTED")
		if($("#rejected").prop('checked')) filterStatus.push("REJECTED")
		if($("#waiting").prop('checked')) filterStatus.push("WAITING")
		if($("#closed").prop('checked')) filterStatus.push("CLOSED")
		/*
		console.log("Filter Values:");
		console.log(filterName);
		console.log(filterId);
		console.log(filterAgeFrom);
		console.log(filterAgeTo);
		console.log(filterStatus);
		console.log(filterType);
		console.log(filterDateFrom);
		console.log(filterDateTo);
		console.log("=========================");
		*/
		var tr = $("#Oncology-patients-table tr");

		tr.each(function(index, tr_elem) {
			if(index != 0) {
				var tds = $(tr_elem).find("td");

				var name = tds[0].innerText.toUpperCase();
				var nid = tds[1].textContent.toUpperCase();
				var age = parseInt(tds[2].textContent);
				//var sex = tds[3].textContent.toUpperCase();
				var status = tds[4].textContent.toUpperCase();
				var type = tds[5].textContent.toUpperCase();
				var date = new Date(tds[6].textContent);
				var doctor = tds[7].textContent.toUpperCase();

				/*
				console.log("Patient info:");
				console.log(name);
				console.log(nid);
				console.log(age);
				console.log(status);
				console.log(type);
				console.log(date);
				console.log("=========================");
				*/
				var filterNameRes = filterName == "" ? true : name.indexOf(filterName.toUpperCase()) > -1;
				var filterIdRes = filterId == "" ? true : nid.indexOf(filterId) > -1;
				var filterAgeFromRes = filterAgeFrom == "" ? true : age >= filterAgeFrom;
				var filterAgeToRes = filterAgeTo == "" ? true : age < filterAgeTo;
				var filterTypeRes = filterType == "ALL" ? true : type.indexOf(filterType.toUpperCase()) > -1;
				var filterMyRes = filterMy == false ? true : doctor == $(".Logged-in .username").text().toUpperCase();
				var filterStatusRes = filterStatus.length == 0 ? true : filterStatus.includes(status.toUpperCase());
				var filterDateFromRes = filterDateFrom == null ? true : date.getDate() >= (new Date(filterDateFrom)).getDate();
				var filterDateToRes = filterDateTo == null ? true : date.getDate() < (new Date(filterDateTo)).getDate();
				/*
				console.log("Filter Results:");
				console.log(filterNameRes);
				console.log(filterIdRes);
				console.log(filterAgeFromRes);
				console.log(filterAgeToRes);
				console.log(filterTypeRes);
				console.log(filterStatusRes);
				console.log(filterDateFromRes);
				console.log(filterDateToRes);
				console.log("=========================");
				*/
				tds.each(function(index, td_elem) {
					if (filterNameRes && filterIdRes && filterAgeFromRes && filterAgeToRes && filterTypeRes && filterMyRes && filterStatusRes && filterDateFromRes && filterDateToRes) {
						$(tr_elem).css('display', "");
					}
					else {
						$(tr_elem).css('display', "none");
					}
				})
			}
		})
	});
}
