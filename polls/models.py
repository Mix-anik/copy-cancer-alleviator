from django.db import models
import json
import ast

class Patient(models.Model):
    nid = models.CharField(max_length=20, primary_key=True)
    name = models.CharField(max_length=50)
    status = models.CharField(max_length=20)
    age = models.CharField(max_length=3)
    phone = models.CharField(max_length=20)
    email = models.CharField(max_length=50)
    info = models.CharField(max_length=5000, default="")
    testings = models.CharField(max_length=5000, default="None")

    def getAge(self):
        return self.age

    def setTestings(self, testings):
        self.testings = str(testings)
    def getTestings(self):
        return self.fromJSON("testings")

class Oncologist_Patient(models.Model):
    nid = models.ForeignKey(Patient, related_name='patient_nid', on_delete=models.PROTECT)
    status = models.CharField(max_length=20)
    #date_added = models.DateTimeField(auto_now_add=True, blank=False, null = False)
    sex=models.CharField(max_length=6)
    date_added =  models.CharField(max_length=50)
    cancer = models.CharField(max_length=50)
    doctor = models.CharField(max_length=50, default="-")
    #need to change this in the future#
    name = models.CharField(max_length=50)
    age = models.CharField(max_length=3)
    phone = models.CharField(max_length=20)
    email = models.CharField(max_length=50)
    info = models.CharField(max_length=5000, default="")
    testings = models.CharField(max_length=5000, default="None")



class PathwayManager(models.Manager):
    def moreThan(self, percentage, symptoms, tests):
        return self.filter(self.calcPercentage(symptoms, tests) > percentage)

    def calcAll(self, symptoms, tests):
        return (self.type, self.calcPercentage(symptoms, percentage))


class Pathway(models.Model):
    patient = None
    type = models.CharField(max_length=20)
    SYMPTOMS = models.CharField(max_length=5000, default="None")
    TESTS = models.CharField(max_length=5000, default="None")
    COMBINATIONS = models.CharField(max_length=5000, default="None")
    objects = PathwayManager()


    def test(self, syms, tess):
        mergedData = syms + tess

        if self.patient != None:
            mergedData += [{'name': 'Age', 'value': self.patient.age}]

        print(mergedData)

        for comb in self.getCombs():
            combResult = all(c in mergedData  for c in comb)
            print(comb)
            print(combResult)

            if combResult:
                return True
        return False


    def hasSymptom(self, symptom):
        for sym in self.getSyms():
            if sym['name'] == symptom.getName():
                return True
        return False

    def hasTest(self, test):
        for tes in self.getTests():
            if tes['name'] == test.getName():
                return True
        return False

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)
    def fromJSON(self, key):
        return json.loads(self.toJSON())[key]

    def setSyms(self, syms):
        self.SYMPTOMS = str(syms)
    def getSyms(self):
        return ast.literal_eval(self.fromJSON("SYMPTOMS"))

    def setTests(self, tests):
        self.TESTS = str(tests)
    def getTests(self):
        return ast.literal_eval(self.fromJSON("TESTS"))

    def setCombs(self, combs):
        self.COMBINATIONS = str(combs)
    def getCombs(self):
        return ast.literal_eval(self.fromJSON("COMBINATIONS"))
