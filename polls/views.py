from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.models import User, auth
from .models import *
import json
import urllib.parse
from django.utils.encoding import uri_to_iri
import ast
from django.shortcuts import render
from django.views.generic import ListView
import django_tables2 as tables
from django_tables2 import SingleTableView

class Symptom:
    def __init__(self,name,value):
        self.name = name
        self.value = value

    def getName(self):
        return self.name

    def getValue(self):
        return self.value

class Test:
    def __init__(self,name,value):
        self.name = name
        self.value = value

    def getName(self):
        return self.name

    def getValue(self):
        return self.value

class Combination:
    def __init__(self,combination):
        self.combination = combination

    def getComb(self):
        return self.combination

def createTests():
    tes1 = "DRE"
    tes2 = "PSA"

    return [tes1, tes2]

def createSymptoms():
    sm1 = "Noktuuria"
    sm2 = "sagenenud-urineerimine"
    sm3 = "kusemise alustamise raskendatus"
    sm4 = "äkitselt tekkiv urineerimivajadus"
    sm5 = "uriiniretensioon"
    sm6 = "seletamatu erektrioonihäire"
    sm7 = "seletamatu makrohematuuria"

    return [sm1, sm2, sm3, sm4, sm5, sm6, sm7]

def createCombinations():
    cb1 = [{'name': 'noktuuria', 'value': True}, {'name': 'DRE', 'value': 1.6}]
    cb2 = [{'name': 'sagenenud-urineerimine', 'value': True}, {'name': 'PSA', 'value': 1.8}]
    cb3 = [{'name': 'Age', 'value': '60'}]

    return [cb1, cb2, cb3]

def ptws(request):
    pw1 = Pathway()
    pw1.type = "Prostate Cancer"
    pw1.SYMPTOMS = createSymptoms()
    pw1.TESTS = createTests()
    pw1.COMBINATIONS = createCombinations()
    pw1.save()

    print(pw1.toJSON())

    for sym in pw1.getSyms():
        print(sym['name'])
        print(sym['value'])

    for tes in pw1.getTests():
        print(tes['name'])
        print(tes['value'])
    print(pw1.getCombs())

    return redirect("/home")


def login(request):
    if request.user.is_authenticated:
        return render(request, 'patients.html')
    elif request.method == "POST":
        _username = request.POST.get('uname')
        _password = request.POST.get('pswd')
        _remember = request.POST.get('remember')

        #print("%s %s %s" % (_username, _password, _remember))
        user = auth.authenticate(request, username=_username, password=_password)

        if user is not None:
            auth.login(request, user)
            #print("Authenticated as: " + user.first_name + " " + user.last_name)

            if not _remember:
                request.session.set_expiry(0)

            #print("Session expire on close: ", request.session.get_expire_at_browser_close())

            return redirect("/patients")
        else:
            print(False)
            return redirect("/home")
    else:
        print("failed")
        return render(request, 'index.html')

def logout(request):
    if request.user.is_authenticated:
        auth.logout(request)
    return redirect("/home")#render(request, 'index.html')

#For Administrator
def register(request):
    if request.method == "POST":
        _firstname = request.POST.get('fname')
        _lastname = request.POST.get('lname')
        _email = request.POST.get('email')
        _username = request.POST.get('uname')
        _password = request.POST.get('pswd')
        _password_rep = request.POST.get('pswd_repeat')

        #print("%s %s %s %s %s" % (_firstname, _lastname, _email, _username, _password))

        if _password == _password_rep and not User.objects.filter(username=_username).exists() and not User.objects.filter(email=_email).exists():
            newUser = User.objects.create_user(username=_username, password=_password, email=_email, first_name=_firstname, last_name=_lastname)
            newUser.save()
        return redirect("/register")

    else:
        return render(request, 'register.html')

def testing(request):
    if request.user.is_authenticated:
        if request.method == "POST" and urllib.parse.unquote(str(uri_to_iri(request.body))) == "Prostate-cancer":
            encodedData = request.body
            decodedData = urllib.parse.unquote(str(uri_to_iri(encodedData)))
            cType = decodedData

            print("CANCER TYPE: ", cType)
            cPathways = Pathway.objects.filter(type = cType)
            cData = cPathways[0]

            data = {
                "symptoms": cData.getSyms(),
                "tests": cData.getTests()
            }

            dataJSON = json.dumps(data)

            return HttpResponse(dataJSON, content_type="application/json")

        elif request.method == "POST":
            encodedData = request.body
            decodedData = urllib.parse.unquote(str(uri_to_iri(encodedData)))
            jsonData = json.loads(decodedData)

            symptoms = []
            tests = []

            #Pathway.objects.all().delete()

            #print("DATA: ")
            #print(jsonData)

            #print("SYMPTOMS: ")
            '''
            for sym in jsonData['symptoms']:
                symptoms.append(Symptom(sym['name'], sym['value']))
                #print(sym['name'])
                #print(sym['value'])

            #print("TESTS: ")
            for tes in jsonData['tests']:
                tests.append(Test(tes['name'], tes['value']))
                #print(tes['name'])
                #print(tes['value'])
            '''

            #Adding patients manually
            '''
            p1 = Patient()
            p1.nid = "133556"
            p1.name = "Name Surname"
            p1.status = "Enrolled"
            p1.age = "65"
            p1.phone = "+37255667788"
            p1.email = "test@gmail.com"
            p1.save()

            p2 = Patient()
            p2.nid = "133558"
            p2.name = "Surname Name"
            p2.status = "Waiting"
            p2.age = "60"
            p2.phone = "+37233445566"
            p2.email = "testing@gmail.com"
            p2.save()

            #Patient.objects.all().delete()
            '''

            patient = Patient.objects.filter(nid = jsonData['nid'])

            pw = Pathway()
            pw.patient = None if len(patient) == 0 else patient[0]
            pw.type = 'Prostate-cancer'
            #pw.SYMPTOMS = createSymptoms()
            #pw.TESTS = createTests()
            #pw.COMBINATIONS = createCombinations()

            pw.setSyms(createSymptoms())
            pw.setTests(createTests())
            pw.setCombs(createCombinations())
            pw.save()

            resultValue = pw.test(jsonData['symptoms'], jsonData['tests'])
            result = {'type':pw.type, 'value': 'Positive' if resultValue else 'Negative'}
            resultJSON = json.dumps(result)

            #print("Result: ", value)
            return HttpResponse(resultJSON, content_type="application/json")
        else:
            return render(request, 'testing.html')

    else:
        return redirect("/home")

def patients(request):
    if request.user.is_authenticated:
        if request.method == "POST":
            encodedData = request.body
            decodedData = urllib.parse.unquote(str(uri_to_iri(encodedData)))
            jsonData = json.loads(decodedData)

            print("YES, JSON DATA:")
            print(jsonData)

            patients = Patient.objects.filter(nid = jsonData['nid'])

            if len(patients) == 0:
                return HttpResponse("Error", content_type="text")
            else:
                patient = patients[0]
                testData = {
                    'type': jsonData['type'],
                    'value': jsonData['value']
                }

                patient.setTestings(testData)
                patient.save()

            return HttpResponse("Success", content_type="text")
        else:
            patient = Patient.objects.filter(nid = "133558")[0]
            print(patient.testings)
            pass
            #TODO: Fetch data from database and insert it into patients modal viewing menu
            #      Also check if testings information exists and add it to patient's "modal-Testing" div
        #print("Is Authenticated")
        return render(request, 'patients.html')
    else:
        #print("Is not Authenticated")
        return redirect("/home")

def oncologist_patients(request):
    if request.user.is_authenticated:
        #TODO: Check if that works
        if request.method == "POST":
            patient_nid = request.body
            print(patient_nid)
            patient = Patient.objects.filter(nid = nid)[0]
            doctor_name = request.user.first_name[0] + "." + request.user.last_name
            patient.doctor = doctor_name
            #patient.save()

            return HttpResponse(doctor_name, content_type="text")

        #print("Is Authenticated")
        return render(request, 'oncologist_patients.html')
    else:
        #print("Is not Authenticated")
        return redirect("/home")


def oncologist_my_patients(request):
    if request.user.is_authenticated:
        #print("Is Authenticated")
        return render(request, 'oncologist_my_patients.html')
    else:
        #print("Is not Authenticated")
        return redirect("/home")


def manager(request):
    if request.user.is_authenticated:
        #print("Is Authenticated")
        return render(request, 'pathway_manager.html')
    else:
        #print("Is not Authenticated")
        return redirect("/home")


class PatientTable(tables.Table):
    class Meta:
        model = Patient
        attrs = {"id": "patients-table"}
        fields = ("name", "nid", "status")

class PatientListView(SingleTableView):
    model = Patient
    table_class = PatientTable
    template_name = 'patients.html'

class Oncologist_PatientTable(tables.Table):
    class Meta:
        model = Oncologist_Patient
        model2 = Patient
        attrs = {"id": "Oncology-patients-table"}
        fields = ("name", "nid", "age", "sex", "status", "cancer", "date_added", "doctor")

class oncologistPatientListView(SingleTableView):
    model = Oncologist_Patient
    table_class = Oncologist_PatientTable
    template_name = 'oncologist_patients.html'
