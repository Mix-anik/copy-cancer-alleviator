"""cancer_alleviator URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import include, url
from polls import views

urlpatterns = [
    url(r'^$', views.login, name='index'),
    url(r'^home/$', views.login, name='index'),
    url(r'^testing/$', views.testing, name='testing'),
    url(r'^manager/$', views.manager, name='manager'),


    url(r'^oncologist_my_patients/$', views.oncologist_my_patients, name='oncologist_my_patients'),
    url(r'^register/$', views.register, name='register'),
    url(r'^logout/$', views.logout, name='logout'),
    url(r'^admin/', admin.site.urls),
    path("patients/", views.PatientListView.as_view()),
    path("oncologist_patients/", views.oncologistPatientListView.as_view())
]
